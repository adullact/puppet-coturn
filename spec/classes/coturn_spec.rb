require 'spec_helper'

describe 'coturn' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) { facts }

      fqdn_server = facts[:networking]['fqdn']

      it { is_expected.to compile }
      it { is_expected.to contain_service('coturn') }
      it { is_expected.to contain_file('/etc/turnserver.conf').with_content(%r{realm=#{fqdn_server}}) }
      it { is_expected.to contain_file('/etc/turnserver.conf').that_notifies('Service[coturn]') }

      context 'with given listening_ips' do
        let(:params) do
          {
            listening_ips: ['1.2.3.4', '5.6.7.8'],
          }
        end

        it { is_expected.to contain_file('/etc/turnserver.conf').with_content(%r{listening-ip=1.2.3.4}) }
        it { is_expected.to contain_file('/etc/turnserver.conf').with_content(%r{listening-ip=5.6.7.8}) }
      end

      context 'with given cert and private_key' do
        let(:params) do
          {
            cert: '/etc/cert/example.com.pem',
            private_key: '/etc/cert/pkey_example.com.pem',
          }
        end

        it { is_expected.to contain_file('/etc/turnserver.conf').with_content(%r{cert=/etc/cert/example.com.pem}) }
        it { is_expected.to contain_file('/etc/turnserver.conf').with_content(%r{pkey=/etc/cert/pkey_example.com.pem}) }
        it { is_expected.to contain_file('/etc/turnserver.conf').with_content(%r{tls-listening-port=5349}) }
        it { is_expected.to contain_file('/etc/turnserver.conf').with_content(%r{no-tlsv1_1}) }
        it { is_expected.to contain_file('/etc/turnserver.conf').with_content(%r{no-tlsv1}) }
        it { is_expected.to contain_file('/etc/turnserver.conf').with_content(%r{dh2066}) }
      end

      context 'with given cert only' do
        let(:params) do
          {
            cert: '/etc/cert/example.com.pem',
          }
        end

        it { is_expected.to compile.and_raise_error(%r{With TLS a certificate and a private key are both required}) }
      end

      context 'with given private_key only' do
        let(:params) do
          {
            private_key: '/etc/cert/pkey_example.com.pem',
          }
        end

        it { is_expected.to compile.and_raise_error(%r{With TLS a certificate and a private key are both required}) }
      end
    end
  end
end
