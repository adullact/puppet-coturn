# Changelog

All notable changes to this project will be documented in this file.

## Release 2.2.0

 * add parameter to handle no-tlsv1_2 option #30
 * add parameters to handle TLS cipher suite and switch default Diffie-Hellman usage to true #29

## Release 2.1.0

 * fix usage of static-auth-secret option #25
 * ensure coturn config file is not world redeable #27
 * add parameter to handel the running user and group of the turn process #26
 * add parameter to handle log-file option #24

## Release 2.0.0

 * add ubuntu20.04 debian10 as supported OS, drop ubuntu16.04 debian9 #21
 * set a default value to realm #18
 * allow puppetlabs-stdlib < 9.x #22
 * add Puppet 7 support , drop Puppet 5 #20
 * include gems pdk and puppet-strings and maintenance update #19

## Release 1.0.2

  * add tags in `metadata.json` file #16

## Release 1.0.1

  * metadata does not permit to get quality scone to 5 #14

## Release 1.0.0

  Initial release

