# coturn

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with coturn](#setup)
    * [Setup requirements](#setup-requirements)
    * [Beginning with coturn](#beginning-with-coturn)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

The Puppet module installs and configures a TURN service with Coturn.

## Setup

### Setup Requirements

Module dependencies are listed in `metadata.json` file.

### Beginning with coturn

As minimalistic setup, it is enought to give a realm :

```
  class { 'coturn' :
    realm => 'coturn.example.org',
  }

```

## Usage

For instance, we can change listening port and restrict the binded ip addresses :

```
  class { 'coturn' :
    realm          => 'coturn.example.org',
    listening_port => 8080,
    listening_ips  => [
      '10.0.0.10',
    ],
  }

```

For WebRTC adding the 3 following parameters are required :
```
  class { 'coturn' :
    lt_cred_mech       => true,
    use_auth_secret    => true,
    static_auth_secret => 'alongstringsharedsecretwithwebrtcserver',
  }
```

Notes :
  * Since the current version of Puppet module does not modify tables in SQLite database,
the dynamic auth method permited by Coturn can not be used. So we have to use the static auth method.
  * In WebRTC usage it should be interesting to bind on 443 port to pass through firewalls. Since,
port 443 is lower than 1024, the defaut non root user `proc_user` will not be able to open socket.
So we will have to use `proc_user` parameter to `root`.

## Reference

Details are in `REFERENCE.md` file.

## Limitations

Supported OSes are given is `metadata.json` file.

## Development

Home at URL https://gitlab.adullact.net/adullact/puppet-coturn

Issues and MR are welcome.

## Release Notes/Contributors/Etc.

Details in `CHANGELOG.md`.

```
Copyright (C) 2018 Association des Développeurs et Utilisateurs de Logiciels Libres
                     pour les Administrations et Colléctivités Territoriales.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/agpl.html>.

```

