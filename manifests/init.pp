# @summary Install and configure Coturn
#
# Install and configure Coturn
#
# @example
#   class { 'coturn':
#     realm =>  'example.com',
#   }
#
# @param realm
#   Name (fully qualified domain name) of the Coturn server
# @param listening_ips
#   Listener IP addresses of relay server. Multiple listeners can be specified.
#   If no IP(s) specified, then all IPv4 and IPv6 system IPs will be used for listening.
# @param listening_port
#   TURN listener port for UDP and TCP (plain).
# @param tls_listening_port
#    TURN listener port for TLS
#    The TURN server "automatically" recognizes the type of traffic.
#    Actually, two listening endpoints (the "plain" one and the "tls" one) are equivalent
#    in terms of functionality. Keeping both endpoints satisfy the RFC 5766 specs.
# @param cert
#   The certificate file use with TLS.
# @param private_key
#   The private key file use with TLS.
# @param cipher_list
#   Allowed OpenSSL cipher list for TLS/DTLS connections.
#   The default value gives a list of ciphers that the Nmap ssl-enum-ciphers script notes A in january 2022.
# @param dh2066
#   Use 2066 bits predefined DH TLS key, size of the key is 1066.
# @param fingerprint
#   Use fingerprints in the TURN messages
# @param lt_cred_mech
#   Use long-term credential mechanism
# @param use_auth_secret
#   Turn on secret-based authentication.
# @param static_auth_secret
#   Static authentication secret value (typically a long hexadecimal string) for TURN REST API only.
#   If not set, then the turn server will try to use the dynamic value in turn_secret table in user database
#   Note : The actual Puppet module version does not support to modify the turn_secret table in database.
# @param use_auth_secret
#   Flag that sets a special WebRTC authorization option that is based upon authentication secret.
#   The actual value of the secret is defined by option static-auth-secret,
#   Note : The actual Puppet module version does not support to modify the turn_secret table in database.
# @param total_quota
#   Total allocation quota
# @param bps_capacity
#   Maximum server capacity.
#   Total bytes-per-second bandwidth the TURN server is allowed to allocate
#   for the sessions, combined (input and output network streams are treated separately).
# @param stale_nonce
#   Add for extra security. Set this option to limit the nonce lifetime, in seconds.
#   After the delay, the client will get 438 error and will have to re-authenticate itself
# @param no_stdout_log
#   Flag to prevent stdout log messages.
#   By default, all log messages are going to both stdout and to
#   the configured log file. With this option everything will be
#   going to the configured log only (unless the log file itself is stdout).
# @param no_loopback_peers
#   Flag that can be used to disallow peers on the loopback addresses (127.x.x.x and ::1).
#   This is an extra security measure.
# @param no_multicast_peers
#   Flag that can be used to disallow peers on well-known broadcast addresses (224.0.0.0 and above, and FFXX:*).
#   This is an extra security measure.
# @param dh2066
#   Use 2066 bits predefined DH TLS key. Default size of the key is 1066.
# @param tlsv1
#   Allow an TLS/DTLS version of protocol v1
# @param tlsv1_1
#   Allow an TLS/DTLS version of protocol v1.1
# @param tlsv1_2
#   Allow an TLS/DTLS version of protocol v1.2
# @param simple_log
#   This flag means that no log file rollover will be used, and the log file
#   name will be constructed as-is, without PID and date appendage.
#   This option can be used together with the logrotate tool.
# @param proc_user
#   User name to run the process, after the initialization of turnserver.
# @param proc_group
#   Group name to run the process, after the initialization of turnserver.
# @param log_file
#   Full path name of the log file or special names.
#   The special names are :
#     * "stdout" that force everything to the stdout.
#     * "syslog" that redirect everything into syslog.
class coturn (
  Stdlib::Fqdn $realm = $facts['networking']['fqdn'],
  Array[Stdlib::IP::Address]  $listening_ips = [],
  Stdlib::Port                $listening_port     = 3478,
  Stdlib::Port                $tls_listening_port = 5349,
  Optional[Stdlib::Absolutepath] $cert = undef,
  Optional[Stdlib::Absolutepath] $private_key = undef,
  Boolean                     $fingerprint        = false,
  Boolean                     $lt_cred_mech       = false,  # Only on Coturn below v4.5.0.8
  Boolean                     $use_auth_secret    = false,
  Optional[String[1]]         $static_auth_secret = undef,
  Integer[0]                  $total_quota        = 0,
  Integer[0]                  $bps_capacity       = 0,
  Integer[0]                  $stale_nonce        = 600,
  Boolean                     $no_stdout_log      = false,
  Boolean                     $no_loopback_peers  = false,   # Only on Coturn below v4.5.1.0!
  Boolean                     $no_multicast_peers = false,
  Boolean $dh2066 = true,
  Boolean $tlsv1 = false,
  Boolean $tlsv1_1 = false,
  Boolean $tlsv1_2 = true,
  String[1] $proc_user = 'turnserver',
  String[1] $proc_group = 'turnserver',
  Boolean $simple_log = true,
  Optional[Variant[Stdlib::Absolutepath,Enum['syslog','stdout']]] $log_file = '/var/log/coturn/turnserver.log',
  String[1] $cipher_list = 'ECDH+AESGCM:ECDH+CHACHA20:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS',
) {

  $_coturn_package = 'coturn' # Ubuntu 18.04 ==> Coturn v4.5.0.7
  $_coturn_config = '/etc/turnserver.conf'
  $_coturn_sqlitedbpath = '/var/lib/turn'

  if ( $cert and ! $private_key ) or ( ! $cert and $private_key ) {
    fail('With TLS a certificate and a private key are both required.')
  }

  package { $_coturn_package:
    ensure => present,
  }

  # Config file
  file { $_coturn_config :
    ensure  => file,
    mode    => '0600',
    owner   => $proc_user,
    group   => $proc_group,
    content => epp('coturn/turnserver.conf.epp'),
    require => Package[$_coturn_package],
    notify  => Service['coturn'],
  }

  # SYS-V init scripts are yet shiped with deb package
  file { ['/etc/default/coturn', '/etc/init.d/coturn']:
    ensure => absent,
  }

  # By default users are stored in sqlite database
  # the turnserver must access to database file
  file { $_coturn_sqlitedbpath:
    ensure => directory,
    mode   => '0700',
    owner  => $proc_user,
    group  => $proc_group,
  }
  -> file { "${_coturn_sqlitedbpath}/turndb":
    ensure => file,
    owner  => $proc_user,
    group  => $proc_group,
  }

  if $facts['os']['release']['major'] == '18.04' {
    # No systemd unit shiped at all
    systemd::unit_file { 'coturn.service':
      content => epp('coturn/coturn.service.ubuntu1804.epp'),
    }

    Systemd::Unit_file['coturn.service'] ~> Service['coturn']
  } else {
    # Systemd unit shiped but we need to manage some settings with dropin file
    systemd::dropin_file { 'coturn.conf':
      unit    => 'coturn.service',
      content => epp('coturn/coturn_dropin.epp'),
    }

    Systemd::Dropin_file['coturn.conf'] ~> Service['coturn']
  }

  if assert_type(Stdlib::Absolutepath, $log_file) {
    file { dirname($log_file):
      ensure => directory,
      mode   => '0700',
      owner  => $proc_user,
      group  => $proc_group,
    }
  }

  service { 'coturn':
    ensure  => running,
    enable  => true,
    require => [
      Package[$_coturn_package],
      File[$_coturn_config],
    ]
  }
}
