require 'spec_helper_acceptance'

ip_server = fact('networking.ip')
fqdn_server = fact('networking.fqdn')
log_file = '/var/log/coturn/turnserver.log'

describe 'coturn' do
  context 'with defaults' do
    pp = %(
      include coturn
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe package('coturn') do
      it { is_expected.to be_installed }
    end

    describe service('coturn') do
      it { is_expected.to be_running }
    end

    describe process('turnserver') do
      its(:user) { is_expected.to eq 'turnserver' }
    end

    describe file('/etc/turnserver.conf') do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 600 }
      its(:content) { is_expected.to contain 'listening-port=3478' }
      its(:content) { is_expected.to contain "realm=#{fqdn_server}" }
    end

    describe file('/etc/init.d/coturn') do
      it { is_expected.not_to be_file }
    end

    describe port(3478) do
      it { is_expected.to be_listening.on('127.0.0.1') }
      it { is_expected.to be_listening.on(ip_server.to_s) }
    end

    describe file(log_file.to_s) do
      it { is_expected.to be_file }
      its(:content) { is_expected.to contain 'log file opened' }
      its(:content) { is_expected.not_to contain 'runtime error' }
    end
  end

  context 'with custom settings' do
    pp = %(
      class { 'coturn' :
        realm          => 'coturn.example.org',
        listening_port => 88,
        listening_ips  => ['#{ip_server}'],
        proc_user      => 'root',
        proc_group     => 'root',
      }
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe service('coturn') do
      it { is_expected.to be_running }
    end

    describe process('turnserver') do
      its(:user) { is_expected.to eq 'root' }
    end

    describe file('/etc/turnserver.conf') do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 600 }
      its(:content) { is_expected.to contain 'listening-port=88' }
      its(:content) { is_expected.to contain 'realm=coturn.example.org' }
    end

    describe port(88) do
      it { is_expected.not_to be_listening.on('127.0.0.1') }
      it { is_expected.to be_listening.on(ip_server.to_s) }
    end
  end
end
